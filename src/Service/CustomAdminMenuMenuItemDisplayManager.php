<?php

namespace Drupal\custom_admin_menu\Service;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Provides tools to hide/display menu items.
 */
class CustomAdminMenuMenuItemDisplayManager {

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'custom_admin_menu.menu_item_display_manager';

  /**
   * Current User.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Current language id.
   *
   * @var string
   */
  protected $currentLanguageId;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    LanguageManagerInterface $language_manager,
  ) {
    $this->currentUser = $current_user;
    $this->currentLanguageId = $language_manager->getCurrentLanguage()->getId();
  }

  /**
   * The singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me(): static {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function initMenuItemsDisplayState(&$variables) {
    foreach ($variables as $pluginId => &$item) {
      if ($this->itemIsDisallowed($item)) {
        unset($variables[$pluginId]);
      }
      else {
        if (isset($item['below'])) {
          $this->initMenuItemsDisplayState($item['below']);
        }
      }
    }
  }

  /**
   * Return true if item is explicitly disallowed.
   *
   * @param array $item
   *   The item.
   * @param \Drupal\Core\Session\AccountProxyInterface|null $user
   *   The user.
   *
   * @return bool
   *   The state.
   */
  protected function itemIsDisallowed(array $item, AccountProxyInterface $user = NULL): bool {
    $user = $user ?: $this->currentUser;
    $is_disallowed = FALSE;
    // Super user.
    if ($this->isSuperUser($user)) {
      return FALSE;
    }

    if (isset($item['original_link'])) {
      $plugin_definition = $item['original_link']->getPluginDefinition();

      if ($plugin_definition['menu_name'] !== CustomAdminMenuManager::CUSTOM_MENU_NAME) {
        return FALSE;
      }

      if (!empty($plugin_definition)) {
        $metadata = isset($plugin_definition['metadata']) ? $plugin_definition['metadata'] : [];
        if (!empty($metadata)) {
          $is_disallowed =
            $this->isDisallowedRoles($metadata, $user)
            || $this->isDisallowedLanguage($metadata);
        }
      }
    }

    return $is_disallowed;
  }

  /**
   * Check item roles.
   *
   * @param array $metadata
   *   The metadata.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user.
   *
   * @return bool
   *   The status.
   */
  protected function isDisallowedRoles(array $metadata, AccountProxyInterface $user): bool {
    $is_disallowed = FALSE;

    if (isset($metadata['disallowed_roles']) && !empty($metadata['disallowed_roles'])) {
      $is_disallowed = 0 < count(
          array_intersect($user->getRoles(), $metadata['disallowed_roles'])
        );
    }
    elseif (isset($metadata['need_roles']) && !empty($metadata['need_roles'])) {
      $is_disallowed = 0 == count((
        array_intersect($user->getRoles(), $metadata['need_roles'])
        ));
    }

    return $is_disallowed;
  }

  /**
   * Check if language is allowed.
   *
   * @param array $metadata
   *   The metadata.
   *
   * @return bool
   *   The status.
   */
  protected function isDisallowedLanguage(array $metadata): bool {
    $is_disallowed = FALSE;

    if (isset($metadata['allowed_languages']) && !empty($metadata['allowed_languages'])) {
      $is_disallowed = !in_array($this->currentLanguageId, $metadata['allowed_languages']);
    }

    return $is_disallowed;
  }

  /**
   * Filter item access.
   *
   * @param array $items
   *   THe list of menu items.
   */
  public function filterItems(array &$items) {
    $items = array_filter($items, function ($item) {
      return !$this->itemIsDisallowed($item);
    });

    foreach ($items as &$item) {
      if (isset($item['below']) && is_array($item['below']) && !empty($item['below'])) {
        $this->filterItems($item['below']);
      }
    }
  }

  /**
   * Return true if user is super user.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface|null $user
   *   The user.
   *
   * @return bool
   *   The user is super user;
   */
  public function isSuperUser(AccountProxyInterface $user = NULL) {
    $user = $user ?: $this->currentUser;
    return $user->id() == 1;
  }

}
