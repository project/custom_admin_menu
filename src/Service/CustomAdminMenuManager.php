<?php

namespace Drupal\custom_admin_menu\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\Container;

/**
 * Service providing tools to render custom admin menu in toolbar.
 *
 * @package Drupal\custom_admin_menu\Service
 */
class CustomAdminMenuManager implements TrustedCallbackInterface {

  use StringTranslationTrait;

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'custom_admin_menu.manager';

  /**
   * Custom Menu Name.
   *
   * @const string
   */
  const CUSTOM_MENU_NAME = 'custom_admin_menu';

  /**
   * Menu link tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Theme admin.
   *
   * @var \Drupal\Core\Theme\ActiveTheme
   */
  protected $adminTheme;

  /**
   * Theme Manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Theme Systeme conf.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $themeSystemConf;

  /**
   * Theme initialisation.
   *
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected $themeInitialization;

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The settings.
   *
   * @var \Drupal\custom_admin_menu\Service\CustomAdminMenuSettings
   */
  protected $settings;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Menu item display manager.
   *
   * @var \Drupal\custom_admin_menu\Service\CustomAdminMenuMenuItemDisplayManager
   */
  protected CustomAdminMenuMenuItemDisplayManager $itemDisplayManager;

  /**
   * CustomAdminMenuManager constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   The menu link tree.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $theme_initialization
   *   The theme intialization.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\custom_admin_menu\Service\CustomAdminMenuSettings $settings
   *   The settings.
   * @param \Drupal\custom_admin_menu\Service\CustomAdminMenuMenuItemDisplayManager $display_item_manager
   *   The display item manager.
   */
  public function __construct(
    AccountInterface $current_user,
    MenuLinkTreeInterface $menu_link_tree,
    ThemeManagerInterface $theme_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ThemeInitializationInterface $theme_initialization,
    ModuleHandlerInterface $module_handler,
    CustomAdminMenuSettings $settings,
    CustomAdminMenuMenuItemDisplayManager $display_item_manager
  ) {
    $this->currentUser = $current_user;
    $this->menuLinkTree = $menu_link_tree;
    $this->adminTheme = $theme_manager->getActiveTheme();
    $this->themeManager = $theme_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->themeSystemConf = $config_factory->get('system.theme');
    $this->themeInitialization = $theme_initialization;
    $this->moduleHandler = $module_handler;
    $this->settings = $settings;
    $this->itemDisplayManager = $display_item_manager;
  }

  /**
   * The singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Check if use can see Default Admin Menu.
   *
   * @return bool
   *   The status.
   */
  public function userCanSeeDefaultAdminMenu() {
    return $this->currentUser->hasPermission('access_default_menu');
  }

  /**
   * Check if user can see custom admin menu.
   *
   * @return bool
   *   The status.
   */
  public function userCanSeeCustomAdminMenu() {
    return $this->currentUser->hasPermission('access_custom_menu');
  }

  /**
   * Return the custom menu content.
   */
  public function getCustomMenuContent(): array {
    // Get the menu.
    $menu = $this->entityTypeManager->getStorage('menu')
      ->load($this->getCustomMenuName());

    // Build the menu.
    $build_array = $this->getMenuBuildArray($menu->id());

    if (isset($build_array['#items']) && is_array($build_array['#items'])) {
      // Filter allowed items.
      $this->itemDisplayManager->filterItems($build_array['#items']);

      // Add toolbar-icon class.
      $this->initClasses($build_array['#items']);
    }
    // Alter custom menu build array.
    $this->moduleHandler->alter('custom_admin_menu', $build_array);
    $this->themeManager->alter('custom_admin_menu', $build_array);

    return $build_array;
  }

  /**
   * Return the menu name.
   *
   * @return string
   *   The menu name.
   */
  public function getCustomMenuName(): string {
    return static::CUSTOM_MENU_NAME;
  }

  /**
   * Return a menu build array.
   *
   * @param string $menuId
   *   L'id du menu.
   * @param array $manipulators
   *   Les manipulators.
   *
   * @return array
   *   Le markup.
   */
  public function getMenuBuildArray(
    string $menuId,
    array $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ]
  ): array {

    $parameters = new MenuTreeParameters();
    $parameters->excludeRoot()->onlyEnabledLinks();
    $tree = $this->menuLinkTree->load($menuId, $parameters);

    $build_elements = $this->menuLinkTree->transform($tree, $manipulators);

    return $this->menuLinkTree->build($build_elements);
  }

  /**
   * Add default classes.
   *
   * @param array $links
   *   Links.
   */
  protected function initClasses(array $links = []) {
    foreach ($links as $item) {
      $id = $item['original_link']->getPluginId();
      $suffix = explode('.', $id);
      $id = str_replace('.', '-', Container::underscore($item['original_link']->getPluginId()));
      /** @var \Drupal\Core\Url $url */
      if ($url = $item['url']) {
        $url->setOption('attributes', [
          'class' => [
            'toolbar-icon',
            'toolbar-icon-' . $id,
          ],
        ]);
      }

      // Modules and themes can update custom menu items element.
      $context = [
        'plugin_id' => $id,
        'suffix' => end($suffix),
      ];
      $this->moduleHandler
        ->alter('custom_admin_menu_item', $item, $context);
      $this->themeManager->alter('custom_admin_menu_item', $item, $context);

      if (isset($item['below']) && is_array($item['below'])) {
        $this->initClasses($item['below']);
      }
    }
  }

  /**
   * Add the contrib menu items.
   *
   * @param array $items
   *   The menu items.
   */
  public function addCustomAdminMenuEntries(array &$items): void {
    $menu = $this->entityTypeManager->getStorage('menu')
      ->load($this->getCustomMenuName());

    if (!$menu) {
      return;
    }

    $menu_name = $this->getCustomMenuName();
    $items[$menu_name] = [
      '#attributes' => ['class' => [$menu_name]],
      '#type' => 'toolbar_item',
      '#weight' => 0,
      'tab' => [
        '#type' => 'link',
        '#title' => $this->t($menu_name),
        '#url' => Url::fromRoute('<front>'),
        '#attributes' => [
          'title' => $menu->label(),
          'class' => ['toolbar-icon', 'toolbar-icon-system-admin-content'],
        ],
      ],
      'tray' => [
        '#heading' => $menu->label(),
        '#attached' => [
          'library' => [
            'admin_toolbar/toolbar.tree',
          ],
        ],
        'toolbar_ui_additions_toolbar_content' => [
          '#pre_render' => [
            [static::class, 'buildCustomToolbarContent'],
          ],
          [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['toolbar-menu-administration'],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Return the contrib menu content.
   *
   * @param array $element
   *   The built element.
   *
   * @return array
   *   The build.
   */
  public static function buildCustomToolbarContent(array $element): array {
    return static::me()->getMenuBuildArray(static::me()->getCustomMenuName());
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'buildCustomToolbarContent',
    ];
  }

}
