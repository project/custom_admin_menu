<?php

namespace Drupal\custom_admin_menu\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Custom Admin Menu routes.
 */
class CustomAdminMenuController extends ControllerBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * ToolbarRedirectController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entityType manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The requestStack.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Redirect to node edit form selected by the query params.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function nodeEditFormRedirect() {
    return $this->entityEditFormRedirect('node');
  }

  /**
   * Redirect to term edit form selected by the query params.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function termEditFormRedirect() {
    return $this->entityEditFormRedirect('taxonomy_term');
  }

  /**
   * Redirect to the edit form of the last entity matching.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function entityEditFormRedirect(string $entity_type): RedirectResponse {
    $entities = [];
    try {
      $entities = $this->getEntities($entity_type);
    }
    catch (\Exception $e) {
    }
    $entity = end($entities);
    if ($entity) {
      return $this->redirect('entity.' . $entity_type . '.edit_form', [$entity_type => $entity->id()]);
    }

    return $this->redirect('system.404');
  }

  /**
   * Return the entity list according to query parameters.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntities(string $entity_type) {
    // Define a property to a field via separator ':'.
    $query = $this->currentRequest->query->all();
    $query = array_combine(array_map(function ($key) {
      return str_replace(':', '.', $key);
    }, array_keys($query)), $query);

    // If a property is an array, get the entity query.
    foreach ($query as $queryElement) {
      if (is_array($queryElement)) {
        return $this->getEntitiesByEntityQuery($entity_type, $query);
      }
    }

    // Return the entities according to properties.
    return $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties($query);
  }

  /**
   * Return the list of entities according to query elements.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array $queryElements
   *   The query elements.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntitiesByEntityQuery(string $entity_type, array $queryElements): array {
    $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
    foreach ($queryElements as $field => $queryElement) {
      if (is_array($queryElement)) {
        $query->condition($field, $queryElement['value'], $queryElement['type']);
      }
      else {
        $query->condition($field, $queryElement);
      }
    }

    return $this->entityTypeManager->getStorage($entity_type)
      ->loadMultiple($query->accessCheck(FALSE)->execute());
  }

}
